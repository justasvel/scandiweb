<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="styles.css">
    <title>
        <?=$title?>
    </title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid border-bottom m-3">
                <a class="navbar-brand" href="#">Product List</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <button class="btn btn-primary m-2" id="saveButton" href="#" disabled>Save</button>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-primary m-2" href="index.php">Cancel</a>
                        </li>
                    </ul>
                </div>
            </div>
    </nav>
    <main>
        <div class="container">
            <?=$output?>
        </div>
    </main>
    <footer>
        <p class="text-center border-top m-3">Scandiweb Test assignment</p>
    </footer>
<script src="js/addproduct.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>