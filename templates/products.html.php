<form action="deleteproducts.php" method="post" class="text-center" id="deleteForm">
    <input type="submit" name="deleteProducts" id="deleteProduct" class="hidden">   
    <?php foreach ($products as $product): ?>
            <div class="card my-2">
                <div class="card-body">
                        <p class="card-text text-center"><?php echo htmlspecialchars($product['sku'], ENT_QUOTES, 'UTF-8') ?></p>
                        <p class="card-text text-center"><?php echo htmlspecialchars($product['name'], ENT_QUOTES, 'UTF-8') ?></p>
                        <p class="card-text text-center"><?php echo htmlspecialchars($product['price'], ENT_QUOTES, 'UTF-8') . '$'?></p>

                        <?php if (!is_null($product['size'])): ?>
                        <p class="card-text text-center"><?php echo 'Size: ' . htmlspecialchars($product['size'], ENT_QUOTES, 'UTF-8') . ' MB'?></p>
                        <?php endif; ?>

                        <?php if (!is_null($product['dimensions'])): ?>
                        <p class="card-text text-center"><?php echo 'Dimensions: ' . htmlspecialchars($product['dimensions'], ENT_QUOTES, 'UTF-8')?></p>
                        <?php endif; ?>

                        <?php if (!is_null($product['weight'])): ?>
                        <p class="card-text text-center"><?php echo 'Weight: ' . htmlspecialchars($product['weight'], ENT_QUOTES, 'UTF-8') . ' KG'?></p>
                        <?php endif; ?>
                        <input type="checkbox" name="checkbox[]" class="text-center" value="<?=$product['sku']?>">
                </div>
            </div>
    <?php endforeach; ?>
</form>