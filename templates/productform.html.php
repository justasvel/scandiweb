<form method="POST" action="">
  <div class="mb-3" id="skuSection">
    <label for="sku" class="form-label">SKU</label>
    <input type="text" class="form-control w-50" id="sku" name="sku" aria-describedby="skuNumber" required>
    <div id="skuNumber" class="form-text">SKU number</div>
  </div>
  <div class="mb-3" id="nameSection">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control w-50" id="name" name="name" required>
  </div>
  <div class="mb-3" id="priceSection">
    <label for="price" class="form-label">Price ($)</label>
    <input type="number" step="0.01" class="form-control w-50" id="price" name="price" required>
  </div>
  <select class="form-select w-25 mb-3" aria-label="Default select example">
    <option selected disabled>Type Swithcer</option>
    <option value="1">Disc</option>
    <option value="2">Book</option>
    <option value="3">Furniture</option>
  </select>

  <!-- Types -->

  <div class="hidden" id="disc-type">
    <label for="disc" class="form-label" id="disc-label">Size (MB)</label>
    <input type="number" step="0.01" class="form-control w-50" id="disc" name="size" aria-describedby="disc-description" required>
    <div id="disc-description" class="form-text">Please enter Disc size in (MB)</div>
  </div>

  <div class="mb-3 hidden" id="furniture-type">
    <label for="book" class="form-label">Dimensions (CM)</label>
    <input type="text" class="form-control w-50" id="furniture" name="dimensions" aria-describedby="furniture-description" required pattern="(\d+(?:.\d+)?)x(\d+(?:.\d+)?)(?:x(\d+(?:.\d+)?))?" title="HxWxL">
    <div id="furniture-description" class="form-text">Please enter Furniture dimensions in the following format HxWxL</div>
  </div>

  <div class="mb-3 hidden" id="book-type">
    <label for="book" class="form-label">Weight (KG)</label>
    <input type="number" step="0.01" class="form-control w-50" id="book" name="weight" aria-describedby="book-description" required>
    <div id="book-description" class="form-text">Please enter Book weight in (KG)</div>
  </div>

  <button type="submit" class="btn btn-primary hidden" id="submit" name="addproduct">Submit</button>
</form>