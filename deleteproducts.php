<?php 

if(isset($_POST['deleteProducts']) && !empty($_POST['deleteProducts'])) {
    try {
        $pdo = new PDO('mysql:host=localhost;dbname=scandiweb;charset=utf8', 'root', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $ids = array();

        foreach($_POST['checkbox'] as $pval) {

            $query = $pdo->prepare("DELETE FROM `products` WHERE `sku` IN ( '$pval' )");
            $query->execute();
        }

        header('location: index.php');

    } catch (PDOException $e) {
        $title = 'An error has occured';

        $output = 'Database error: ' . $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();
    }
} else {
    $title = 'Product list';

    ob_start();

    include __DIR__ . './templates/products.html.php';

    $output = ob_get_clean();
}

include __DIR__ . '/templates/index.html.php';