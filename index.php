<?php

try {
    //Connect to the database
    $pdo = new PDO ('mysql:host=localhost;dbname=scandiweb;charset=utf8', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'SELECT * FROM `products`';

    $products = $pdo->query($sql);

    $title = 'Product list'; 

    //Starts output buffer.
    ob_start();

    include __DIR__ . './templates/products.html.php';
//Stores contents of output buffer into a variable
    $output = ob_get_clean();
    
} catch (PDOException $e) {
    $title = 'An error has occurred';
    $error = 'Database error: ' . $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();
}
include __DIR__ . '/templates/index.html.php';