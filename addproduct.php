<?php

if (isset($_POST['addproduct'])) {
    try {
        $pdo = new PDO ('mysql:host=localhost;dbname=scandiweb;charset=utf8', 'root', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        if (isset($_POST['size']) && !empty($_POST['size'])) {

            $sql = 'INSERT INTO `products` SET
            `sku` = :sku,
            `name` = :name,
            `price` = :price,
            `size` = :size';

            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':sku', $_POST['sku']);
            $stmt->bindValue(':name', $_POST['name']);
            $stmt->bindValue(':price', $_POST['price']);
            $stmt->bindValue(':size', $_POST['size']);

            $stmt->execute();

            header('location: index.php');

        } else if (isset($_POST['dimensions']) && !empty($_POST['dimensions'])) {

            $sql = 'INSERT INTO `products` SET
            `sku` = :sku,
            `name` = :name,
            `price` = :price,
            `dimensions` = :dimensions';

            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':sku', $_POST['sku']);
            $stmt->bindValue(':name', $_POST['name']);
            $stmt->bindValue(':price', $_POST['price']);
            $stmt->bindValue(':dimensions', $_POST['dimensions']);

            $stmt->execute();

            header('location: index.php');

        } else if (isset($_POST['weight']) && !empty($_POST['weight'])) {

            $sql = 'INSERT INTO `products` SET
            `sku` = :sku,
            `name` = :name,
            `price` = :price,
            `weight` = :weight';

            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':sku', $_POST['sku']);
            $stmt->bindValue(':name', $_POST['name']);
            $stmt->bindValue(':price', $_POST['price']);
            $stmt->bindValue(':weight', $_POST['weight']);

            $stmt->execute();

            header('location: index.php');
        } else {
            
            header('location: index.php');
        }

    } catch (PDOException $e) {
        $title = 'An error has occured';

        $output = 'Database error: ' . $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();
    }
} else {
    $title = 'Add a new product';

    ob_start();

    include __DIR__ . './templates/productform.html.php';

    $output = ob_get_clean();
}

include __DIR__ . './templates/addproduct.html.php';