//Select field
const selection = document.querySelector('.form-select');
//Disc item
const discInput = document.querySelector('#disc');
const discDiv = document.querySelector('#disc-type');
//Book item
const bookInput = document.querySelector('#book');
const bookDiv = document.querySelector('#book-type');
//Furniture item 
const furnitureInput = document.querySelector('#furniture');
const furnitureDiv = document.querySelector('#furniture-type');
//Submit button
const saveButton = document.querySelector('#saveButton');

//Displays input fields based on switcher selection
selection.addEventListener('change', (e) => {
    if (selection.value == 1) {
        discInput.type = 'text';
        discDiv.setAttribute('class', 'mb-3');

        bookInput.type = 'hidden';
        bookDiv.setAttribute('class', 'hidden');

        furnitureInput.type = 'hidden';
        furnitureDiv.setAttribute('class', 'hidden');
    } else if (selection.value == 2) {
        bookInput.type = 'text';
        bookDiv.setAttribute('class', 'mb-3');

        discInput.type = 'hidden';
        discDiv.setAttribute('class', 'hidden');

        furnitureInput.type = 'hidden';
        furnitureDiv.setAttribute('class', 'hidden');
    } else {
        furnitureInput.type = 'text';
        furnitureDiv.setAttribute('class', 'mb-3');

        discInput.type = 'hidden';
        discDiv.setAttribute('class', 'hidden');

        bookInput.type = 'hidden';
        bookDiv.setAttribute('class', 'hidden');
    }

    saveButton.disabled = false;
});

//Save button functionality

saveButton.addEventListener('click', () => {
    document.getElementById('submit').click();
});