// Mass Delete button functionality
const massDeleteBtn = document.querySelector('#massDeleteBtn');

massDeleteBtn.addEventListener('click', () => {
    document.getElementById('deleteProduct').click();
});