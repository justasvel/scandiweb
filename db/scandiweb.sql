-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2021 at 11:54 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `size` float DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `dimensions` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`sku`, `name`, `price`, `size`, `weight`, `dimensions`) VALUES
('JVC200123', 'Acme Disc', 1, 700, NULL, NULL),
('JVC200124', 'Acme Disc', 1, 700, NULL, NULL),
('JVC200125', 'Acme Disc', 1, 700, NULL, NULL),
('JVC200126', 'Acme Disc', 1, 700, NULL, NULL),
('GGWP0007', 'War and Peace', 20, NULL, 2, NULL),
('GGWP0008', 'War and Peace', 20, NULL, 2, NULL),
('GGWP0009', 'War and Peace', 20, NULL, 2, NULL),
('GGWP0010', 'War and Peace', 20, NULL, 2, NULL),
('TR120555', 'Chair', 40, NULL, NULL, '24x45x15'),
('TR120556', 'Chair', 40, NULL, NULL, '24x45x15'),
('TR120557', 'Chair', 40, NULL, NULL, '24x45x15'),
('TR120558', 'Chair', 40, NULL, NULL, '24x45x15');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
